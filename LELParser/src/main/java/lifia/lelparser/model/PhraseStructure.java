package lifia.lelparser.model;

import java.util.ArrayList;
import java.util.Collection;
import lifia.lelparser.model.phraseContent.PhraseContent;

/**
 * Represents the contents of a phrase (subjets, verbs, etc.)
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class PhraseStructure {

    private ArrayList<PhraseContent> phraseContents;

    public PhraseStructure() {
        this.phraseContents = new ArrayList<>();
    }

    /**
     * @return the subjects
     */
    public ArrayList<PhraseContent> getSubjects() {
        ArrayList<PhraseContent> result = new ArrayList<>();

        this.phraseContents.stream().filter((currentContent) -> (currentContent.getType().equals("subject"))).forEach((currentContent) -> {
            result.add(currentContent);
        });

        return result;
    }

    /**
     * @return the verbs
     */
    public ArrayList<PhraseContent> getVerbs() {
        ArrayList<PhraseContent> result = new ArrayList<>();

        this.phraseContents.stream().filter((currentContent) -> (currentContent.getType().equals("verb"))).forEach((currentContent) -> {
            result.add(currentContent);
        });

        return result;
    }

    public void addSubject(String newSubject, UserStory usRef) {
        Collection<UserStory> usCollection = new ArrayList<>();
        usCollection.add(usRef);
        this.phraseContents.add(new PhraseContent("subject", newSubject, usCollection));
    }

    public void addVerb(String newVerb, UserStory usRef) {
        Collection<UserStory> usCollection = new ArrayList<>();
        usCollection.add(usRef);
        this.phraseContents.add(new PhraseContent("verb", newVerb, usCollection));
    }

    public void addObject(String newObj, UserStory usRef) {
        Collection<UserStory> usCollection = new ArrayList<>();
        usCollection.add(usRef);
        this.phraseContents.add(new PhraseContent("object", newObj, usCollection));
    }

    /**
     * @return the objects
     */
    public ArrayList<PhraseContent> getObjects() {
        ArrayList<PhraseContent> result = new ArrayList<>();

        this.phraseContents.stream().filter((currentContent) -> (currentContent.getType().equals("object"))).forEach((currentContent) -> {
            result.add(currentContent);
        });

        return result;
    }

}
