package lifia.lelparser.model.parserResults;

import java.util.ArrayList;
import java.util.Collection;
import lifia.lelparser.model.UserStory;
import lifia.lelparser.model.phraseContent.PhraseContent;

/**
 *
 * @author Juan Ignacio Tonelli <jtonelli@lifia.info.unlp.edu.ar>
 */
public class ParserResult {

    private Collection<UserStory> userStories;
    protected Collection<PhraseContent> content;

    public ParserResult() {
        this.userStories = new ArrayList<>();
        this.content = new ArrayList<>();
    }

    /**
     * @return the content
     */
    public Collection<PhraseContent> getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(Collection<PhraseContent> content) {
        this.content = content;
    }

    public void addContents(ArrayList<PhraseContent> contents) {
        contents.stream().forEach((currContent) -> {
            this.addOrUpdateSinglePhraseContent(currContent);
        });
    }

    private void addOrUpdateSinglePhraseContent(PhraseContent nuevoCont) {
        boolean alreadyContained = false;

        for (PhraseContent currContent : this.getContent()) {
            {
                if (currContent.equals(nuevoCont)) {
                    currContent.setAppearances(currContent.getAppearances() + 1);
                    currContent.getUsReferences().addAll(nuevoCont.getUsReferences());
                    alreadyContained = true;
                    break;
                }
            }
        }

        if (!alreadyContained) {
            this.getContent().add(nuevoCont);
        }

    }

    /**
     * @return the userStories
     */
    public Collection<UserStory> getUserStories() {
        return userStories;
    }

    /**
     * @param userStories the userStories to set
     */
    public void setUserStories(Collection<UserStory> userStories) {
        this.userStories = userStories;
    }

}
