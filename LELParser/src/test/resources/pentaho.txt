#http://jira.pentaho.com/issues/?jql=text%20~%20%22i%20want%20to%22#

# User stories from DPI module

As an ETL-Developer, I want to define new database dialects and change the behaviour of existing ones more easily without Java coding
As a ETL developer I want to be able to set the case (in)sensitivity of a string field.
As an ETL developer, I want Ease of Use Improvements (EOU DI Repository)
As an ETL Designer/Administrator, I want a repository migration utility so I can easily move transformation/jobs from one location to another.
As an ETL manager I want to have a step that gives me information about the content of a repository
As an ETL Designer, I want the ability to paste multiple columns of text into any tables in step configuration dialogs
As an ETL-Designer, I want to generate a UUID
As an Administrator, I want the ability to monitor metrics using a 3rd party SNMP console (Phase 1, SNMP traps)
As an ETL Designer, I want an improved method for handling NULL values in the Dimension Lookup/Update step.
As a MongoDB Developer I want to be able to run findAndModify
I want an option to ignore case in the Stream Lookup
As an ETL developer I want to support Bloom filters
As an ETL embedder I want to detect if a transformation is idle or not.
As an ETL-Developer, I want the default save comments be configurable (also blank) and on each commit selectable from previous 10 comments (DI repository)
As an Administrator, I want the ability to monitor a DI-/Carte server using a 3rd party SNMP console (Phase 2, SNMP Polls)
As a Metadata Injection User, I want support for the JSON Output Step
As an ETL Designer/ETL Developer, I want the PDI Documentation available in the Wiki
I want to be able to run a sequence of steps in a single thread
As an ETL developer, I want the slave server to accept both get and post
As an ETL developer, I want to be able to connect to SAP HANA natively
As an OpenERP output step user I want to be able to specify the XML ID
As an ETL developer I want a transformation step to create/update OpenERP objects
As an ETL-Administrator, I want to know the username that started a job or transformation
As an ETL Developer, I want to be able to use Special Characters as Delimiters
As an ETL Designer, I want a step which validates Addresses
As an ETL Developer, I want Checkpoints in Sub-Jobs to be respected
As an ETL Developer I want Global Variables for the Checkpoint Log
As an ETL Designer, I want my graphs to be layed out automatically
As an ETL developer I want a transformation step to delete OpenERP objects
As an ETL Developer, I want to be able to add seconds in the Calculator Step
As an Administrator, I want to have an Administration perspective in the DI-Server
As an ETL Designer, I want a step which validates email addresses
As a ETL Designer, I want the ability to filter logs by individual threads
As an Administrator, I want a clean-up functionality of log-entries in the logging datamart
As a Plugin Developer, I want to be able to develop plugins that are dependent on the Hadoop Shim
As an ETL Designer, I want manageable 'Favorites' for Steps and Job Entries
As an ETL-Developer, I want a workflow system that supports dependencies
As a Hadoop User, I want usability improvements in Pentaho MapReduce
I want to show the execution status of the sub-jobs when I am using the parent-job
As a ETL designer, I would like to be able to choose if I want to import of connection properties when importing from file
As an ETL Designer/Administrator, I want version control on my jobs and transformations
As an ETL-Developer, I want my jobs support restartability
As an ETL designer I want to have detailed timing metrics
As an ETL developer, I want the ability to abort a transformation when no rows are in the stream
